var PORT = process.env.PORT || 5000;

var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);

server.listen(PORT);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

var users = {};

// For heroku. disable websocket
io.configure(function () {
  io.set("transports", ["xhr-polling"]);
  io.set("polling duration", 10);
});

io.sockets.on('connection', function (socket) {
  socket.on('message', function (data) {
    io.sockets.emit('message', data);
  });

  socket.on('disconnect', function () {
    io.sockets.emit('message', { writer: 'system', text: users[socket.id].name + ' leaved.'});
    delete users[socket.id];
    io.sockets.emit('user-refresh', users);
  });

  socket.on('join', function (data) {
    users[socket.id] = { name: data.name };
    io.sockets.emit('message', { writer: 'system', text: data.name + ' joinned.'});
    io.sockets.emit('user-refresh', users);
  })
});
